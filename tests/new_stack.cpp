#include <sure/stack/new.hpp>

#include <wheels/test/test_framework.hpp>

TEST_SUITE(NewStack) {
  SIMPLE_TEST(AllocateBytes) {
    auto stack = sure::NewStack::AllocateBytes(1024 * 1024);
    char* bottom = stack.MutView().End();

    *(bottom - 100) = 0xFF;
  }

  SIMPLE_TEST(MoveConstructible) {
    auto stack1 = sure::NewStack::AllocateBytes(1024 * 1024);
    auto stack2 = std::move(stack1);
  }
}
